<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Direction
 *
 * @ORM\Table(name="direction", indexes={@ORM\Index(name="exam_second_id", columns={"exam_second_id"}), @ORM\Index(name="exam_fifth_id", columns={"exam_fifth_id"}), @ORM\Index(name="faculty_id", columns={"faculty_id"}), @ORM\Index(name="exam_third_id", columns={"exam_third_id"}), @ORM\Index(name="exam_first_id", columns={"exam_first_id"}), @ORM\Index(name="exam_fourth_id", columns={"exam_fourth_id"})})
 * @ORM\Entity
 */
class Direction
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=255, nullable=false)
     */
    private $name;

    /**
     * @var int
     *
     * @ORM\Column(name="rating", type="integer", nullable=false)
     */
    private $rating;

    /**
     * @var int|null
     *
     * @ORM\Column(name="free_places", type="integer", nullable=true)
     */
    private $freePlaces;

    /**
     * @var int
     *
     * @ORM\Column(name="min_point_free", type="integer", nullable=false)
     */
    private $minPointFree;

    /**
     * @var int|null
     *
     * @ORM\Column(name="paid_places", type="integer", nullable=true)
     */
    private $paidPlaces;

    /**
     * @var int|null
     *
     * @ORM\Column(name="min_point_paid", type="integer", nullable=true)
     */
    private $minPointPaid;

    /**
     * @var int|null
     *
     * @ORM\Column(name="payment", type="integer", nullable=true)
     */
    private $payment;

    /**
     * @var int|null
     *
     * @ORM\Column(name="privilege_places", type="integer", nullable=true)
     */
    private $privilegePlaces;

    /**
     * @var \Faculty
     *
     * @ORM\ManyToOne(targetEntity="Faculty")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="faculty_id", referencedColumnName="id")
     * })
     */
    private $faculty;

    /**
     * @var \Exam
     *
     * @ORM\ManyToOne(targetEntity="Exam")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="exam_first_id", referencedColumnName="id")
     * })
     */
    private $examFirst;

    /**
     * @var \Exam
     *
     * @ORM\ManyToOne(targetEntity="Exam")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="exam_second_id", referencedColumnName="id")
     * })
     */
    private $examSecond;

    /**
     * @var \Exam
     *
     * @ORM\ManyToOne(targetEntity="Exam")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="exam_third_id", referencedColumnName="id")
     * })
     */
    private $examThird;

    /**
     * @var \Exam
     *
     * @ORM\ManyToOne(targetEntity="Exam")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="exam_fourth_id", referencedColumnName="id")
     * })
     */
    private $examFourth;

    /**
     * @var \Exam
     *
     * @ORM\ManyToOne(targetEntity="Exam")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="exam_fifth_id", referencedColumnName="id")
     * })
     */
    private $examFifth;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getRating(): ?int
    {
        return $this->rating;
    }

    public function setRating(int $rating): self
    {
        $this->rating = $rating;

        return $this;
    }

    public function getFreePlaces(): ?int
    {
        return $this->freePlaces;
    }

    public function setFreePlaces(?int $freePlaces): self
    {
        $this->freePlaces = $freePlaces;

        return $this;
    }

    public function getMinPointFree(): ?int
    {
        return $this->minPointFree;
    }

    public function setMinPointFree(int $minPointFree): self
    {
        $this->minPointFree = $minPointFree;

        return $this;
    }

    public function getPaidPlaces(): ?int
    {
        return $this->paidPlaces;
    }

    public function setPaidPlaces(?int $paidPlaces): self
    {
        $this->paidPlaces = $paidPlaces;

        return $this;
    }

    public function getMinPointPaid(): ?int
    {
        return $this->minPointPaid;
    }

    public function setMinPointPaid(?int $minPointPaid): self
    {
        $this->minPointPaid = $minPointPaid;

        return $this;
    }

    public function getPayment(): ?int
    {
        return $this->payment;
    }

    public function setPayment(?int $payment): self
    {
        $this->payment = $payment;

        return $this;
    }

    public function getPrivilegePlaces(): ?int
    {
        return $this->privilegePlaces;
    }

    public function setPrivilegePlaces(?int $privilegePlaces): self
    {
        $this->privilegePlaces = $privilegePlaces;

        return $this;
    }

    public function getFaculty(): ?Faculty
    {
        return $this->faculty;
    }

    public function setFaculty(?Faculty $faculty): self
    {
        $this->faculty = $faculty;

        return $this;
    }

    public function getExamFirst(): ?Exam
    {
        return $this->examFirst;
    }

    public function setExamFirst(?Exam $examFirst): self
    {
        $this->examFirst = $examFirst;

        return $this;
    }

    public function getExamSecond(): ?Exam
    {
        return $this->examSecond;
    }

    public function setExamSecond(?Exam $examSecond): self
    {
        $this->examSecond = $examSecond;

        return $this;
    }

    public function getExamThird(): ?Exam
    {
        return $this->examThird;
    }

    public function setExamThird(?Exam $examThird): self
    {
        $this->examThird = $examThird;

        return $this;
    }

    public function getExamFourth(): ?Exam
    {
        return $this->examFourth;
    }

    public function setExamFourth(?Exam $examFourth): self
    {
        $this->examFourth = $examFourth;

        return $this;
    }

    public function getExamFifth(): ?Exam
    {
        return $this->examFifth;
    }

    public function setExamFifth(?Exam $examFifth): self
    {
        $this->examFifth = $examFifth;

        return $this;
    }


}

<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Point
 *
 * @ORM\Table(name="point", indexes={@ORM\Index(name="direction_id", columns={"direction_id"})})
 * @ORM\Entity
 */
class Point
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var int
     *
     * @ORM\Column(name="year", type="integer", nullable=false)
     */
    private $year;

    /**
     * @var int
     *
     * @ORM\Column(name="exam_first_point", type="integer", nullable=false)
     */
    private $examFirstPoint;

    /**
     * @var int
     *
     * @ORM\Column(name="exam_second_point", type="integer", nullable=false)
     */
    private $examSecondPoint;

    /**
     * @var int
     *
     * @ORM\Column(name="exam_third_point", type="integer", nullable=false)
     */
    private $examThirdPoint;

    /**
     * @var int|null
     *
     * @ORM\Column(name="exam_fourth_point", type="integer", nullable=true)
     */
    private $examFourthPoint;

    /**
     * @var int|null
     *
     * @ORM\Column(name="exam_fifth_point", type="integer", nullable=true)
     */
    private $examFifthPoint;

    /**
     * @var int
     *
     * @ORM\Column(name="sum", type="integer", nullable=false)
     */
    private $sum;

    /**
     * @var string
     *
     * @ORM\Column(name="tariff", type="string", length=1, nullable=false, options={"fixed"=true})
     */
    private $tariff;

    /**
     * @var \Direction
     *
     * @ORM\ManyToOne(targetEntity="Direction")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="direction_id", referencedColumnName="id")
     * })
     */
    private $direction;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getYear(): ?int
    {
        return $this->year;
    }

    public function setYear(int $year): self
    {
        $this->year = $year;

        return $this;
    }

    public function getExamFirstPoint(): ?int
    {
        return $this->examFirstPoint;
    }

    public function setExamFirstPoint(int $examFirstPoint): self
    {
        $this->examFirstPoint = $examFirstPoint;

        return $this;
    }

    public function getExamSecondPoint(): ?int
    {
        return $this->examSecondPoint;
    }

    public function setExamSecondPoint(int $examSecondPoint): self
    {
        $this->examSecondPoint = $examSecondPoint;

        return $this;
    }

    public function getExamThirdPoint(): ?int
    {
        return $this->examThirdPoint;
    }

    public function setExamThirdPoint(int $examThirdPoint): self
    {
        $this->examThirdPoint = $examThirdPoint;

        return $this;
    }

    public function getExamFourthPoint(): ?int
    {
        return $this->examFourthPoint;
    }

    public function setExamFourthPoint(?int $examFourthPoint): self
    {
        $this->examFourthPoint = $examFourthPoint;

        return $this;
    }

    public function getExamFifthPoint(): ?int
    {
        return $this->examFifthPoint;
    }

    public function setExamFifthPoint(?int $examFifthPoint): self
    {
        $this->examFifthPoint = $examFifthPoint;

        return $this;
    }

    public function getSum(): ?int
    {
        return $this->sum;
    }

    public function setSum(int $sum): self
    {
        $this->sum = $sum;

        return $this;
    }

    public function getTariff(): ?string
    {
        return $this->tariff;
    }

    public function setTariff(string $tariff): self
    {
        $this->tariff = $tariff;

        return $this;
    }

    public function getDirection(): ?Direction
    {
        return $this->direction;
    }

    public function setDirection(?Direction $direction): self
    {
        $this->direction = $direction;

        return $this;
    }


}

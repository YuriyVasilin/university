<?php

namespace App\Form;

use App\Entity\Region;
use App\Entity\University;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormError;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class InsertUniversityType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('region', ChoiceType::class, [
                'choices'  => $options['regions_array'],
                'mapped' => false,
            ])
            ->add('name')
        ;

        $formModifier = function (FormInterface $form, Region $region) {

            if (!$region->getCities()) {

                $form->add('city', ChoiceType::class, [
                    'choices' => [],
                ]);

                $form->addError(new FormError('Этот регион не имеет городов!
                    Пожалуйста, перейдите по ссылке insert/city для добавления городов'));
            }
            else {
                $cities_in_database = $region->getCities();
                foreach($cities_in_database as $city) {
                    $cities[$city->getName()] = $city;
                }

                $form->add('city', ChoiceType::class, [
                    'choices' => $cities,
                ]);

            }
        };

        $builder->addEventListener(
            FormEvents::PRE_SET_DATA,
            function (FormEvent $event) use ($options, $formModifier) {
                $formModifier(
                    $event->getForm(),
                    $options['regions_array'][array_key_first($options['regions_array'])]
                );
            }
        );

        $builder->get('region')->addEventListener(
            FormEvents::POST_SUBMIT,
            function (FormEvent $event) use ($formModifier) {
                $region = $event->getForm()->getData();

                $formModifier($event->getForm()->getParent(), $region);
            }
        );
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => University::class,
            'regions_array' => null,
        ]);

        $resolver->setAllowedTypes('regions_array', 'array');
    }
}

<?php

namespace App\Controller;

use App\Entity\City;
use App\Entity\Region;
use App\Entity\University;
use App\Form\InsertRegionType;
use App\Form\InsertCityType;
use App\Form\InsertUniversityType;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Form\FormError;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Request;

class AdminController extends AbstractController
{
    /**
     * @Route("/admin/insert", name="insert")
     */
    public function insert()
    {
        return $this->render('admin/insert/insert.html.twig');
    }

    /**
     * @Route("/admin/insert/region", name="region")
     */
    public function region(Request $request)
    {
        // Создаём пустой класс региона
        $region = new Region();
        // и форму под него
        $insert_region = $this->createForm(InsertRegionType::class, $region);
        // Отправка формы
        $insert_region->handleRequest($request);
        if ($insert_region->isSubmitted() && $insert_region->isValid()) {
            // ...проверяем, существует ли уже такой регион в базе

            // Делаем запрос в таблицу Regions
            $repository = $this->getDoctrine()->getRepository(Region::class);
            // Ищем в ней строку с таким же полем name, что и в отправленной форме
            $region_in_database = $repository->findOneBy(['name' => $region->getName()]);
            // Если такой элемент уже существует, создаём ошибку
            if ($region_in_database) {
                $insert_region->addError(new FormError('Такой регион уже есть в базе!'));
            }
            // Если такого региона ещё нет, закидываем его в базу
            else {
                $entityManager = $this->getDoctrine()->getManager();
                $entityManager->persist($region);
                $entityManager->flush();
                $this->addFlash(
                    'success',
                    'Регион "' . $region->getName() . '" успешно сохранён!'
                );
            }
        }

        return $this->render('admin/insert/region/region.html.twig', [
            'insert_region' => $insert_region->createView(),
        ]);
    }

    /**
     * @Route("/admin/insert/city", name="city")
     */
    public function city(Request $request)
    {
        // Получаем список регионов
        $regions_in_database = $this->getDoctrine()->getRepository(Region::class)->findAll();

        //Записываем названия регионов в массив
        $regions = [];
        foreach ($regions_in_database as $region) {
            $regions[(string)$region->getName()] = $region;
        }

        // Создаём пустой класс города
        $city = new City();
        // и форму под него
        $insert_city = $this->createForm(InsertCityType::class, $city, [
            'regions_array' => $regions,
        ]);

        // Проверяем наличие регионов
        if (!$regions_in_database) {
            $insert_city->addError(new FormError("Отсутствуют регионы!
                Пожалуйста, перейдите по ссылке /insert/region и добавьте соответствующий регион"));
        }

        // Отправка формы
        $insert_city->handleRequest($request);
        if ($insert_city->isSubmitted() && $insert_city->isValid()) {

            // Проверяем, есть ли уже такой город в базе
            $repository = $this->getDoctrine()->getRepository(City::class);
            $city_in_database = $repository->findOneBy(
                [
                    'name' => $city->getName(),
                    'region' => $city->getRegion(),
                ]
            );

            // Если есть, выдаём ошибку
            if ($city_in_database) {
                $insert_city->addError(new FormError('Такой город уже есть в базе!'));
            }
            else {
                $entity_manager = $this->getDoctrine()->getManager();
                $entity_manager->persist($city);
                $entity_manager->flush();
                $this->addFlash(
                    'success',
                    'Город "' . $city->getName() . '" успешно сохранён!'
                );
            }
        }

        return $this->render('admin/insert/city/city.html.twig', [
            'insert_city' => $insert_city->createView(),
        ]);
    }

    /**
     * @Route("/admin/insert/university", name="university")
     */
    public function university(Request $request)
    {
        // Получаем список регионов
        $regions_in_database = $this->getDoctrine()->getRepository(Region::class)->findAll();

        // Записываем их в массив, где индекс - имя региона
        $regions = [];
        foreach ($regions_in_database as $region) {
            $regions[(string)$region->getName()] = $region;
        }

        // Создаём пустой класс вуза
        $university = new University();
        // и форму под него
        $insert_university = $this->createForm(InsertUniversityType::class, $university, [
            'regions_array' => $regions,
        ]);

        // Проверяем наличие регионов
        if (!$regions_in_database) {
            $insert_university->addError(new FormError("Отсутствуют регионы!
                Пожалуйста, перейдите по ссылке /insert/region и добавьте соответствующий регион"));
        }

        // Отправка формы
        $insert_university->handleRequest($request);
        if ($insert_university->isSubmitted() && $insert_university->isValid()) {

            // Проверяем, есть ли уже такой вуз в базе
            $repository = $this->getDoctrine()->getRepository(University::class);
            $university_in_database = $repository->findOneBy(
                [
                    'name' => $university->getName(),
                    'city' => $university->getCity(),
                ]
            );

            // Если есть, выдаём ошибку
            if ($university_in_database) {
                $insert_university->addError(new FormError('Такой вуз уже есть в базе!'));
            }
            else {
                $university->setRating(0);
                $entity_manager = $this->getDoctrine()->getManager();
                $entity_manager->persist($university);
                $entity_manager->flush();
                $this->addFlash(
                    'success',
                    'Университет "' . $university->getName() . '" успешно сохранён!'
                );
            }
        }

        return $this->render('admin/insert/university/university.html.twig', [
            'insert_university' => $insert_university->createView(),
        ]);
    }
}
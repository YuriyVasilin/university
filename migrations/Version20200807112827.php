<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20200807112827 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE cities CHANGE region region INT NOT NULL');
        $this->addSql('ALTER TABLE directions CHANGE faculty faculty INT NOT NULL');
        $this->addSql('ALTER TABLE faculties CHANGE university university INT NOT NULL, CHANGE category category INT NOT NULL');
        $this->addSql('ALTER TABLE points CHANGE direction direction INT NOT NULL, CHANGE exam_first exam_first INT NOT NULL, CHANGE exam_second exam_second INT NOT NULL, CHANGE exam_third exam_third INT NOT NULL');
        $this->addSql('ALTER TABLE universities CHANGE city city INT NOT NULL');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE cities CHANGE region region INT DEFAULT NULL');
        $this->addSql('ALTER TABLE directions CHANGE faculty faculty INT DEFAULT NULL');
        $this->addSql('ALTER TABLE faculties CHANGE university university INT DEFAULT NULL, CHANGE category category INT DEFAULT NULL');
        $this->addSql('ALTER TABLE points CHANGE direction direction INT DEFAULT NULL, CHANGE exam_first exam_first INT DEFAULT NULL, CHANGE exam_second exam_second INT DEFAULT NULL, CHANGE exam_third exam_third INT DEFAULT NULL');
        $this->addSql('ALTER TABLE universities CHANGE city city INT DEFAULT NULL');
    }
}
